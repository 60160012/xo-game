import java.util.Scanner;

public class XO {

	static char[][] Board = { { ' ', '1', '2', '3' }, { '1', '-', '-', '-' }, { '2', '-', '-', '-' },
			{ '3', '-', '-', '-' } };

	static int num = 0;
	static int turn = 1;
	static int Alerbreak = 0;
	static char player = 'X';

	static void PrintWelcome() {
		System.out.println("Welcome to OX Game");
	}

	static void PrintBoard() {
		for (int i = 0; i < Board.length; i++) {
			for (int j = 0; j < Board[i].length; j++) {
				System.out.print(Board[i][j] + " ");
			}
			System.out.println();
		}

	}

	static void PrintTurn() {
		System.out.println(player + " Turn");
	}

	static void Input() {
		while (true) {
			try {
				Scanner kb = new Scanner(System.in);

				System.out.println("Please input Row Col: ");
				String str = kb.nextLine();
				String[] input = str.split(" ");
				if (input.length != 2) {
					System.out.println("Error to input try again");
					continue;
				}
				int row = Integer.parseInt(input[0]);
				int col = Integer.parseInt(input[1]);
				if (row < 1 || row > 3) {
					System.out.println("Error to input try again");
					continue;
				} else if (col < 1 || col > 3) {
					System.out.println("Error to input try again");
					continue;
				}

				if (Board[row][col] == '-') {
					Board[row][col] = player;
				}
				break;

			} catch (Exception e) {
				System.out.println("Error to input try again");
			}
		}

	}

	static void SwitchTurn() {
		if (player == 'X') {
			player = 'O';
		} else {
			player = 'X';
		}
		turn++;

	}

	static void PrintWin() {
		if (num == 1) {
			System.out.println("X Win");
		} else if (num == 2) {
			System.out.println("O Win");
		} else {
			System.out.println("Draw");
		}
	}

	static void Horizontal() {
		if ((Board[1][1] == Board[1][2] && Board[1][2] == Board[1][3] && Board[1][3] != '-')
				|| (Board[2][1] == Board[2][2] && Board[2][2] == Board[2][3] && Board[2][3] != '-')
				|| (Board[3][1] == Board[3][2] && Board[3][2] == Board[3][3] && Board[3][3] != '-')) {
			if (player == 'X') {
				num = 1;
			} else {
				num = 2;
			}
			Alerbreak = 1;

		}
	}

	static void Dertical() {

		if ((Board[1][1] == Board[2][1] && Board[2][1] == Board[3][1] && Board[3][1] != '-')
				|| (Board[1][2] == Board[2][2] && Board[2][2] == Board[3][2] && Board[3][2] != '-')
				|| Board[1][3] == Board[2][3] && Board[2][3] == Board[3][3] && Board[3][3] != '-') {
			if (player == 'X') {
				num = 1;
			} else {
				num = 2;
			}
			Alerbreak = 1;
		}

	}

	static void Diagonal() {
		if ((Board[1][1] == Board[2][2] && Board[2][2] == Board[3][3] && Board[3][3] != '-')
				|| (Board[3][1] == Board[2][2] && Board[2][2] == Board[1][3] && Board[1][3] != '-')) {
			if (player == 'X') {
				num = 1;
			} else {
				num = 2;
			}
			Alerbreak = 1;

		}
	}

	static void Draw() {
		if (turn == 9) {
			Alerbreak = 1;
		}
	}

	static void ChackWin() {
		Horizontal();
		Dertical();
		Diagonal();
		Draw();

	}

	static void Printbye() {
		System.out.println("bye");
	}

	public static void main(String[] args) {
		PrintWelcome();
		while (true) {
			PrintBoard();
			PrintTurn();
			Input();			
			ChackWin();
			if(Alerbreak==1) {
				break;
			}
			SwitchTurn();
	 
		}
		PrintBoard();
		PrintWin();
		Printbye();

	}

}
